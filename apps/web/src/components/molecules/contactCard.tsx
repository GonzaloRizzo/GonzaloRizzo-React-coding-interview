import { Box, Typography, Avatar } from '@mui/material';
import { SystemStyleObject, Theme } from '@mui/system';

import { Card } from '@components/atoms';
import { IContact } from 'react-coding-interview-shared/models';
import { InlineField } from './InlineField';

export interface IContactCardProps {
  person: IContact;
  sx?: SystemStyleObject<Theme>;
  onUpdate(newContact: IContact): void;
}

export const ContactCard: React.FC<IContactCardProps> = ({
  person,
  onUpdate,
  sx,
}) => {
  const { name, email } = person;
  return (
    <Card sx={sx}>
      <Box display="flex" flexDirection="column" alignItems="center">
        <Avatar />
        <Box textAlign="center" mt={2}>
          <InlineField
            typographyProps={{ variant: 'subtitle1', lineHeight: '1rem' }}
            value={name}
            onChange={(newName) => {
              onUpdate({ ...person, name: newName });
            }}
          />

          <Typography variant="caption" color="text.secondary">
            {email}
          </Typography>
        </Box>
      </Box>
    </Card>
  );
};
