import { Typography, TypographyProps, TextField, Button } from '@mui/material';
import { useState } from 'react';

export interface InlineFieldProps {
  typographyProps: TypographyProps;
  value: string;
  onChange(value: string): void;
}

export function InlineField({
  typographyProps,
  value,
  onChange,
}: InlineFieldProps) {
  const [isEditing, setIsEditing] = useState(false);
  const [internalValue, setInternalValue] = useState(value);

  if (isEditing) {
    return (
      <div>
        <TextField
          variant="standard"
          value={internalValue}
          onChange={(e) => {
            setInternalValue(e.target.value);
          }}
        />
        <Button
          onClick={() => {
            onChange(internalValue);
            setIsEditing(false);
          }}
        >
          done
        </Button>
        <Button onClick={() => setIsEditing(false)}>cancel</Button>
      </div>
    );
  }

  return (
    <Typography
      {...typographyProps}
      onClick={() => {
        setIsEditing(true);
        setInternalValue(value);
      }}
    >
      {value}
    </Typography>
  );
}
